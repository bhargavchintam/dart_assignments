import 'dart:io';
import 'dart:math';

void main() {
  bool checkWinner({required String user, required String computer}) {
    Map<String, String> map = {
      'Rock': 'Paper',
      'Paper': 'Scissors',
      'Scissors': 'Rock'
    };
    return (map[user] != computer) && (user != computer);
  }

  print("Welcome Rock Paper Scissors!!");
  String? s = null;
  List<String> L = ['Rock', 'Paper', 'Scissors'];
  while (true) {
    print('Enter Option Rock/Paper/Scissors: ');
    s = stdin.readLineSync().toString();
    if (s == 'exit') break;
    Random random = new Random();
    int r = random.nextInt(3);
    print('You: $s\nComputer: ${L[r]}');
    if (s == L[r]) {
      print("Tie!!");
      continue;
    }
    checkWinner(user: s, computer: L[r]) == true
        ? print("You Win!!")
        : print("Computer Wins!!");
  }
}

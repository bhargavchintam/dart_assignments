import 'dart:io';

void main() {
  print("Enter your number?");
  int? number = int.parse(stdin.readLineSync()!);
  number % 2 == 0 ? print("$number is Even!") : print("$number is Odd!");
}

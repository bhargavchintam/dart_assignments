import 'dart:io';

void main() {
  print('Enter list: ');
  List<int> even = stdin
      .readLineSync()
      .toString()
      .split(' ')
      .map((i) => int.parse(i))
      .toList();
  even.removeWhere((i) => (i % 2 != 0));
  print('Even List: $even');
}

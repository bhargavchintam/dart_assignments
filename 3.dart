import 'dart:io';

void main() {
  print('Enter the String: ');
  String? s = stdin.readLineSync();
  String? rev = s.toString().split('').reversed.join('');
  s == rev ? print('$s is palindrome!') : print('$s is not palindrome!');
}

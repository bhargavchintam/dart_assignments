import 'dart:io';

void main() {
  int userInput = 2;
  List<List<int>> cumulativeList = [];
  while (userInput > 0) {
    cumulativeList
        .add(parseInputStringToListInt(index: cumulativeList.length + 1));
    userInput--;
  }
  print(
      "Common Elements are: ${getListOfCommonElements(cumulativeList: cumulativeList)}");
}

List<int> parseInputStringToListInt(
    {required int index, String splitter = ' '}) {
  print("Enter List $index: ");
  String? input = stdin.readLineSync();
  return input.toString().split(splitter).map((i) => int.parse(i)).toList();
}

List<int> getListOfCommonElements({required List<List<int>> cumulativeList}) {
  Map<int, int> map = Map();
  cumulativeList.forEach((eachList) => eachList.forEach((eachElement) =>
      map[eachElement] =
          map.containsKey(eachElement) ? (map[eachElement]! + 1) : 1));
  return map.keys.where((key) => map[key] == cumulativeList.length).toList();
}
